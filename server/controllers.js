import fs from 'fs'

const records = [
  {
    title: 'Название',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus corporis accusamus sequi, hic debitis repellendus nisi adipisci! Nemo, in quod!',
    file: 'file.txt'
  }
]

export const getRecords = (req, res) => {
  // fs.readdir('./public/files', function (err, items) {
  //   res.send(items)
  // })
  res.send(records)
}

export const deleteRecord = (req, res) => {
  records.splice(req.params.index, 1)
  res.send()
}

export const createRecord = (req, res) => {
  const { title, description } = req.body 
  const file = req.files.file
  file.mv('public/files/' + file.name, function(err) {
    if (err) {
      return res.status(500).send(err)
    }
    records.push({
      title,
      description,
      file: file.name
    })
    console.log(records);
    res.send()
  })
}