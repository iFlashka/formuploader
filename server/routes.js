import { Router } from 'express';
import { getRecords, createRecord, deleteRecord } from './controllers';

const routes = new Router()

routes.get('/records', getRecords)
routes.post('/records', createRecord)
routes.delete('/records/:index', deleteRecord)

export default routes