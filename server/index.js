import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import cors from 'cors'
import fileupload from 'express-fileupload'

import routes from './routes'

export const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(cors())
app.use(fileupload())
app.use(express.static('public'))

app.use(morgan('dev'))
app.use('/api', routes)

app.listen(8081, function () {
  console.log('Started on port 8081');
})